const Router = require('koa-router');
const bodyParser = require('koa-body');
const logger = require('koa-logger')
const validator = require('koa2-validator');
const uuid = require('uuid')
const jwt = require('jsonwebtoken');

const Koa = require('koa');
const app = new Koa();

app.use(logger());
app.use(bodyParser());
app.use(validator());

const rsaPrivateKey = 
`-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEAvzoCEC2rpSpJQaWZbUmlsDNwp83Jr4fi6KmBWIwnj1MZ6CUQ
7rBasuLI8AcfX5/10scSfQNCsTLV2tMKQaHuvyrVfwY0dINk+nkqB74QcT2oCCH9
XduJjDuwWA4xLqAKuF96FsIes52opEM50W7/W7DZCKXkC8fFPFj6QF5ZzApDw2Qs
u3yMRmr7/W9uWeaTwfPx24YdY7Ah+fdLy3KN40vXv9c4xiSafVvnx9BwYL7H1Q8N
iK9LGEN6+JSWfgckQCs6UUBOXSZdreNN9zbQCwyzee7bOJqXUDAuLcFARzPw1EsZ
AyjVtGCKIQ0/btqK+jFunT2NBC8RItanDZpptQIDAQABAoIBAQCsssO4Pra8hFMC
gX7tr0x+tAYy1ewmpW8stiDFilYT33YPLKJ9HjHbSms0MwqHftwwTm8JDc/GXmW6
qUui+I64gQOtIzpuW1fvyUtHEMSisI83QRMkF6fCSQm6jJ6oQAtOdZO6R/gYOPNb
3gayeS8PbMilQcSRSwp6tNTVGyC33p43uUUKAKHnpvAwUSc61aVOtw2wkD062XzM
hJjYpHm65i4V31AzXo8HF42NrAtZ8K/AuQZne5F/6F4QFVlMKzUoHkSUnTp60XZx
X77GuyDeDmCgSc2J7xvR5o6VpjsHMo3ek0gJk5ZBnTgkHvnpbULCRxTmDfjeVPue
v3NN2TBFAoGBAPxbqNEsXPOckGTvG3tUOAAkrK1hfW3TwvrW/7YXg1/6aNV4sklc
vqn/40kCK0v9xJIv9FM/l0Nq+CMWcrb4sjLeGwHAa8ASfk6hKHbeiTFamA6FBkvQ
//7GP5khD+y62RlWi9PmwJY21lEkn2mP99THxqvZjQiAVNiqlYdwiIc7AoGBAMH8
f2Ay7Egc2KYRYU2qwa5E/Cljn/9sdvUnWM+gOzUXpc5sBi+/SUUQT8y/rY4AUVW6
YaK7chG9YokZQq7ZwTCsYxTfxHK2pnG/tXjOxLFQKBwppQfJcFSRLbw0lMbQoZBk
S+zb0ufZzxc2fJfXE+XeJxmKs0TS9ltQuJiSqCPPAoGBALEc84K7DBG+FGmCl1sb
ZKJVGwwknA90zCeYtadrIT0/VkxchWSPvxE5Ep+u8gxHcqrXFTdILjWW4chefOyF
5ytkTrgQAI+xawxsdyXWUZtd5dJq8lxLtx9srD4gwjh3et8ZqtFx5kCHBCu29Fr2
PA4OmBUMfrs0tlfKgV+pT2j5AoGBAKnA0Z5XMZlxVM0OTH3wvYhI6fk2Kx8TxY2G
nxsh9m3hgcD/mvJRjEaZnZto6PFoqcRBU4taSNnpRr7+kfH8sCht0k7D+l8AIutL
ffx3xHv9zvvGHZqQ1nHKkaEuyjqo+5kli6N8QjWNzsFbdvBQ0CLJoqGhVHsXuWnz
W3Z4cBbVAoGAEtnwY1OJM7+R2u1CW0tTjqDlYU2hUNa9t1AbhyGdI2arYp+p+umA
b5VoYLNsdvZhqjVFTrYNEuhTJFYCF7jAiZLYvYm0C99BqcJnJPl7JjWynoNHNKw3
9f6PIOE1rAmPE8Cfz/GFF5115ZKVlq+2BY8EKNxbCIy2d/vMEvisnXI=
-----END RSA PRIVATE KEY-----`;

const rsaPublicKey =
`-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvzoCEC2rpSpJQaWZbUml
sDNwp83Jr4fi6KmBWIwnj1MZ6CUQ7rBasuLI8AcfX5/10scSfQNCsTLV2tMKQaHu
vyrVfwY0dINk+nkqB74QcT2oCCH9XduJjDuwWA4xLqAKuF96FsIes52opEM50W7/
W7DZCKXkC8fFPFj6QF5ZzApDw2Qsu3yMRmr7/W9uWeaTwfPx24YdY7Ah+fdLy3KN
40vXv9c4xiSafVvnx9BwYL7H1Q8NiK9LGEN6+JSWfgckQCs6UUBOXSZdreNN9zbQ
Cwyzee7bOJqXUDAuLcFARzPw1EsZAyjVtGCKIQ0/btqK+jFunT2NBC8RItanDZpp
tQIDAQAB
-----END PUBLIC KEY-----`;

//------------------------------------------------------------------------
// Utils

// Call with await sleep(10000);
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function randomIntFromInterval(min,max) {
    return Math.floor(Math.random()*(max-min+1)+min);
}

//------------------------------------------------------------------------
// Auth middleware

async function authMiddleware(ctx, next) {

  authHeader = ctx.request.header['authorization']; 
  if (!authHeader) {
    ctx.status = 400;
    ctx.body = {
      status: 400,
      success: false,
      message: 'Missing Authorization header'
    };
    return;
  } 

  let parts = authHeader.split(' ');
  if (parts.length != 2 && "BEARER" != parts[0].toUpperCase() && parts[1].length === 0) {
    ctx.status = 400;
    ctx.body = {
      status: 400,
      success: false,
      message: 'Invalid Authorization header'
    };
    return;
  }

  //console.log("AUTH HEADER: " + authHeader);


  try {
    decoded = await jwt.verify(parts[1], rsaPublicKey, { algorithm: 'RS256'});
  } catch (err) {
    ctx.status = 401;
    ctx.body = {
      status: 401,
      success: false,
      message: 'Invalid access token'
    };
    return;
  }

  await next();
}

//------------------------------------------------------------------------
// GET /auth/keys
async function getKeys(ctx) {
  ctx.body = { 
    status: 200,
    success: true,
    message: 'NOT IMPLEMENTED YET!'
  };
}


//------------------------------------------------------------------------
// POST /auth/token
async function createToken(ctx) {

  // check input data
  // {
  //   client_id: '12345',
  //   client_secret: '67890',
  //   scope: 'none',
  //   grant_type: 'client_credentials' // or 'refresh_token'
  // }

  ctx.checkBody('client_id', 'Invalid request').notEmpty();
  ctx.checkBody('client_secret', 'Invalid request').notEmpty();
  ctx.checkBody('scope', 'Invalid request').optional();
  ctx.checkBody('grant_type', 'Invalid request').notEmpty();

  let errors = ctx.validationErrors();
  if (errors) {
    console.log("----------------------------------------------------");
    console.log("Validation errors:")
    console.log(errors);
    console.log("----------------------------------------------------");
    ctx.status = 400;
    ctx.body = { 
      status: 400,
      success: false,
      message: 'Invalid request'
    };
    return;
  }

  let params = ctx.request.body;
  console.log(params);

  if (params.client_id  === "" || params.client_secret === "" || params.grant_type === "") {
    ctx.status = 401;
    ctx.body = { 
      status: 401,
      success: false,
      message: 'Invalid request'
    };
    console.error("Invalid request");
    return
  }

  if (params.client_id !== '12345') {
    ctx.status = 401;
    ctx.body = { 
      status: 401,
      success: false,
      message: 'Invalid client id'
    };
    console.error("Invalid client id");
    return
  }
  if (params.client_secret !== '67890') {
    ctx.status = 401;
    ctx.body = { 
      status: 401,
      success: false,
      message: 'Invalid client secret'
    };
    console.error("Invalid client secret");
    return
  }

  if (params.grant_type !== 'client_credentials' && params.grant_type !== 'refresh_token') {
    ctx.status = 401;
    ctx.body = { 
      status: 401,
      success: false,
      message: 'Invalid request - wrong grant_type'
    };
    console.error("Invalid request - wrong grant_type");
    return
  }

  if (!params.scope) {
    params.scope = 'none';
  }

  let now = Math.floor(new Date() / 1000);

  let header = {
    alg: 'RS256',
    typ: 'JWT',
    kid: '123456'
  };

  let claims = {
    iss: 'twostep.io',
    iat: now,
    exp: now + 3600,
    aud: params.client_id,
    scope: params.scope
  };
  let token = jwt.sign(claims, rsaPrivateKey, { header: header, algorithm: 'RS256'});

  ctx.body = { 
    access_token: token,
    token_type: 'Bearer',
    scope: params.scope,
    expires_in: 3600, // one hour
    refresh_token: uuid.v4()
  };
}

//------------------------------------------------------------------------

// POST /v1/users
async function createUser(ctx) {

  ctx.checkBody('email', 'Invalid email address').isEmail();
  ctx.checkBody('phone', 'Missing phone number').notEmpty();
  ctx.checkBody('country_code', 'Missing country code').isInt();

  let errors = ctx.validationErrors();
  if (errors) {
    console.log("----------------------------------------------------");
    console.log("Validation errors:")
    console.log(errors);
    console.log("----------------------------------------------------");
    ctx.status = 400;
    ctx.body = { 
      status: 400,
      success: false,
      message: 'Invalid request'
    };
    return;
  }

  let params = ctx.request.body;
  console.log(params);
  if (params.email === "" || params.phone === "") {
    ctx.status = 400;
    ctx.body = { 
      status: 400,
      success: false,
      message: 'Invalid request'
    };
    return
  }

  let id = randomIntFromInterval(100000, 999999);

  ctx.body = { 
    status: 200,
    success: true,
    data: {
      id: id.toString()
    } 
  };
}

//------------------------------------------------------------------------
// GET /v1/users/:id
async function getUser(ctx) {
  if (ctx.params['id'] === '123456') {
    ctx.body = { 
      status: 200,
      success: true,
      data: {
        id: '123456',
        phone: '+370-XXX-XXX-XX15',
        country_code: 370
      } 
    };
  } else {
    ctx.status = 404;
    ctx.body = { 
      status: 404,
      success: false,
      message: 'User not found'
    };
  }
}

//------------------------------------------------------------------------
// POST /v1/users/:id/sms
async function requestSMS(ctx) {
  if (ctx.params['id'] === '123456') {
    ctx.body = { 
      status: 200,
      success: true,
      data: {
        phone: '+370-XXX-XXX-XX15',
      } 
    };
  } else {
    ctx.status = 404;
    ctx.body = { 
      status: 404,
      success: false,
      message: 'User not found'
    };
  }
}

//------------------------------------------------------------------------
// POST /v1/users/:id/call
async function requestCall(ctx) {
  if (ctx.params['id'] === '123456') {
    ctx.body = { 
      status: 200,
      success: true,
      data: {
        phone: '+370-XXX-XXX-XX15',
      } 
    };
  } else {
    ctx.status = 404;
    ctx.body = { 
      status: 404,
      success: false,
      message: 'User not found'
    };
  }
}

//------------------------------------------------------------------------
// DELETE /v1/users/:id
async function deleteUser(ctx) {
  if (ctx.params['id'] === '123456') {
    ctx.body = { 
      status: 200,
      success: true,
      data: {
        id: ctx.params['id']
      }
    };
  } else {
    ctx.status = 404;
    ctx.body = { 
      status: 404,
      success: false,
      message: 'User not found'
    };
  }
}

//------------------------------------------------------------------------
// Auth

let r = new Router();
r.post('/auth/token', createToken);
r.get('/auth/keys', getKeys);
app.use(r.routes());

var v1 = new Router({
  prefix: '/v1'
});

v1.use(authMiddleware);

// Users
v1.post('/users', createUser);
v1.get('/users/:id', getUser);
v1.post('/users/:id/sms', requestSMS);
v1.post('/users/:id/call', requestCall);
v1.delete('/users/:id', deleteUser);

app.use(v1.routes());

app.use(async function pageNotFound(ctx) {
  ctx.status = 404;
  switch (ctx.accepts('json')) {
    case 'json':
      ctx.body = {
        status: 404,
        message: 'Route not found',
        success: false
      };
      break;
    default:
      ctx.type = 'text';
      ctx.body = 'Route not found';
  }
});

//------------------------------------------------------------------------

app.listen(4000);
console.log("Mock server has started on port 4000");